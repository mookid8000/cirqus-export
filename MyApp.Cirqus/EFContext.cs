#region using

using System.Data.Entity;
using MyApp.Cirqus.Views;

#endregion

namespace MyApp.Cirqus
{
    public class EFContext : DbContext
    {
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<SellerView> SellerViews { get; set; }

        public EFContext(string connectionString)
            : base($"name={connectionString}")
        {
        }
    }
}