﻿#region using

using System;
using d60.Cirqus.Views.ViewManagers;
using d60.Cirqus.Views.ViewManagers.Locators;
using MyApp.Cirqus.Model.BaseContext.Events;

#endregion

namespace MyApp.Cirqus.Views
{
    public class SellerView : IViewInstance<InstancePerAggregateRootLocator>,
        ISubscribeTo<AggregateRootNameUpdatedEvent>,
        ISubscribeTo<AggregateRootCreatedEvent>, ISubscribeTo<AggregateRootRemovedEvent>
    {
        public string Name { get; set; }
        public Guid? SellerId { get; set; }

        public DateTimeOffset? Created { get; set; }
        public DateTimeOffset? Deleted { get; set; }

        public void Handle(IViewContext context, AggregateRootCreatedEvent domainEvent)
        {
            Created = domainEvent.Creation;
        }

        public void Handle(IViewContext context, AggregateRootNameUpdatedEvent domainEvent)
        {
            Name = domainEvent.Name;
        }

        public void Handle(IViewContext context, AggregateRootRemovedEvent domainEvent)
        {
            Deleted = domainEvent.Deletion;
        }

        public string Id
        {
            get { return SellerId.ToString(); }
            set { SellerId = new Guid(value); }
        }

        public long LastGlobalSequenceNumber { get; set; }
    }
}