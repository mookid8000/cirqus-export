﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Autofac;
using Autofac.Core;
using d60.Cirqus;
using MyApp.Cirqus.Views;

namespace MyApp.Cirqus
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["db"];
            var cirqusAutoFacModule = new CirqusAutoFacModule(connectionString.ConnectionString, "Events");

            var builder = new ContainerBuilder();

            builder.RegisterModule(cirqusAutoFacModule);

            using (var container = builder.Build())
            {
                var commandProcessor = container.Resolve<ICommandProcessor>();

                var viewInstances = container
                    .Resolve<IQueryable<SellerView>>()
                    .ToList();

                Console.WriteLine("Got {0} view instances!", viewInstances.Count);

                Console.WriteLine("Press ENTER to quit");
                Console.ReadLine();
            }
        }
    }
}