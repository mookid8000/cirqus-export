﻿#region using

using System.Collections.Generic;
using System.Linq;
using Autofac;
using d60.Cirqus;
using d60.Cirqus.EntityFramework;
using d60.Cirqus.Logging;
using d60.Cirqus.MsSql.Config;
using d60.Cirqus.Views;
using d60.Cirqus.Views.ViewManagers;
using MyApp.Cirqus.Views;

#endregion

namespace MyApp.Cirqus
{
    public class CirqusAutoFacModule : Module
    {
        private readonly string _connectionStringName;
        private readonly string _eventsTableName;

        public CirqusAutoFacModule(string connectionStringName, string eventsTableName)
        {
            _connectionStringName = connectionStringName;
            _eventsTableName = eventsTableName;
        }

        protected override void Load(ContainerBuilder builder)
        {
            //builder.Register(c => new EFContext(_connectionStringName));

            // register all views here, duplicate the line below for every view or write a better way to register views
            RegisterEfViewManager<SellerView>(builder, _connectionStringName);

            // register the Cirqus CommandProcessor
            // remarks: it is adviced to use a single CommandProcessor for the complete application
            builder.Register(c =>
            {
                var viewManagers = c.Resolve<IEnumerable<IViewManager>>().ToArray();
                var commandProcessor = CommandProcessor
                    .With()
                    .Logging(l => l.UseConsole(Logger.Level.Debug))
                    .EventStore(e => e.UseSqlServer(_connectionStringName, _eventsTableName))
                    .EventDispatcher(e => e.UseViewManagerEventDispatcher(viewManagers))
                    .Create();
                return commandProcessor;
            }).SingleInstance();

            // register all the services in the services namespace (which implement the IServices)
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => t.Namespace?.EndsWith("Services") ?? false)
                .PropertiesAutowired()
                .AsImplementedInterfaces();
        }

        /// <summary>
        ///     This method registers a view manager in Autofac as IViewManager and makes sures IQueryable resolves for the view
        /// </summary>
        /// <typeparam name="TViewInstance">The ViewManager you want to register</typeparam>
        /// <param name="builder">The Autofac ContainerBuilder</param>
        /// <param name="connStr">Name of the connection string for the ViewManagers</param>
        private static void RegisterEfViewManager<TViewInstance>(ContainerBuilder builder, string connStr)
            where TViewInstance : class, IViewInstance, ISubscribeTo, new()
        {
            var viewManager = new EntityFrameworkViewManager<TViewInstance>(connStr);

            builder.RegisterInstance<IViewManager>(viewManager);

            builder.Register(c => viewManager.CreateContext());

            builder.Register(c => c.Resolve<IDbContext<TViewInstance>>().Views);
        }
    }
}