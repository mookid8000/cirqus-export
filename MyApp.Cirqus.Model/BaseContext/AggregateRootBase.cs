﻿#region using

using System;
using d60.Cirqus.Aggregates;
using d60.Cirqus.Events;
using MyApp.Cirqus.Model.BaseContext.Events;

#endregion

namespace MyApp.Cirqus.Model.BaseContext
{
    public class AggregateRootBase : AggregateRoot, IEmit<AggregateRootCreatedEvent>, IEmit<AggregateRootRemovedEvent>,
        IEmit<AggregateRootNameUpdatedEvent>, IEquatable<AggregateRootBase>
    {
        private DateTimeOffset? _creation;
        private DateTimeOffset? _deletion;
        private string _name;

        public string Name
        {
            get { return _name; }
            set { Emit(new AggregateRootNameUpdatedEvent(value)); }
        }

        public DateTimeOffset? Creation
        {
            get { return _creation; }
            set { Emit(new AggregateRootCreatedEvent(value)); }
        }

        public DateTimeOffset? Deletion
        {
            get { return _deletion; }
            set { Emit(new AggregateRootRemovedEvent(value)); }
        }

        public void Apply(AggregateRootCreatedEvent e)
        {
            _creation = e.Creation;
        }

        public void Apply(AggregateRootNameUpdatedEvent e)
        {
            _name = e.Name;
        }

        public void Apply(AggregateRootRemovedEvent e)
        {
            _deletion = e.Deletion;
        }

        public bool Equals(AggregateRootBase other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Creation.Equals(other.Creation) && Deletion.Equals(other.Deletion) && string.Equals(Name, other.Name);
        }

        protected override void Created()
        {
            Creation = DateTimeOffset.UtcNow;
        }

        public override string ToString()
        {
            return $"{Id} ({Name})";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AggregateRootBase) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _creation.GetHashCode();
                hashCode = (hashCode*397) ^ _deletion.GetHashCode();
                hashCode = (hashCode*397) ^ (_name?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}