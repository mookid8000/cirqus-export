#region using

using System;
using d60.Cirqus.Events;

#endregion

namespace MyApp.Cirqus.Model.BaseContext.Events
{
    public class AggregateRootCreatedEvent : DomainEvent<AggregateRootBase>
    {
        public DateTimeOffset? Creation { get; }

        public AggregateRootCreatedEvent(DateTimeOffset? creation)
        {
            Creation = creation;
        }
    }
}