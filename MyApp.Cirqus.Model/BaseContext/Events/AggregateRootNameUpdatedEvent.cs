﻿#region using

using d60.Cirqus.Events;

#endregion

namespace MyApp.Cirqus.Model.BaseContext.Events
{
    public class AggregateRootNameUpdatedEvent : DomainEvent<AggregateRootBase>
    {
        public string Name { get; }

        public AggregateRootNameUpdatedEvent(string name)
        {
            Name = name;
        }
    }
}