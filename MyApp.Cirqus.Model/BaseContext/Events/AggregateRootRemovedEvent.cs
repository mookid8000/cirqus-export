﻿#region using

using System;
using d60.Cirqus.Events;

#endregion

namespace MyApp.Cirqus.Model.BaseContext.Events
{
    public class AggregateRootRemovedEvent : DomainEvent<AggregateRootBase>
    {
        public DateTimeOffset? Deletion { get; }

        public AggregateRootRemovedEvent(DateTimeOffset? deletion)
        {
            Deletion = deletion;
        }
    }
}