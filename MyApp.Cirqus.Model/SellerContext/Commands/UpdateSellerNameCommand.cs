﻿#region using

using System;
using d60.Cirqus.Commands;

#endregion

namespace MyApp.Cirqus.Model.SellerContext.Commands
{
    public class UpdateSellerNameCommand : Command<Seller>
    {
        private readonly string _newName;

        public UpdateSellerNameCommand(Guid aggregateRootId, string newName) : base(aggregateRootId.ToString())
        {
            _newName = newName;
        }

        public override void Execute(Seller aggregateRoot)
        {
            aggregateRoot.Name = _newName;
        }
    }
}