﻿#region using

using System;

#endregion

namespace MyApp.Cirqus.Model.SellerContext.Commands
{
    public interface ICreateCommand
    {
        Guid CreatedGuid { get; }
    }
}