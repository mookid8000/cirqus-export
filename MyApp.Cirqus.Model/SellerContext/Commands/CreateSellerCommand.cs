﻿#region using

using System;
using d60.Cirqus.Commands;
using MyApp.Cirqus.Model.SharedContext;

#endregion

namespace MyApp.Cirqus.Model.SellerContext.Commands
{
    public class CreateSellerCommand : ExecutableCommand, ICreateCommand
    {
        private readonly Address _address;
        private readonly string _name;

        public CreateSellerCommand(string name, Address address)
        {
            _name = name;
            _address = address;
            CreatedGuid = Guid.NewGuid();
        }

        public CreateSellerCommand(Seller seller) : this(seller.Name, seller.Address)
        {
        }

        public Guid CreatedGuid { get; }

        public override void Execute(ICommandContext context)
        {
            var seller = context.Create<Seller>(CreatedGuid.ToString());
            seller.Name = _name;
            seller.Address = _address;
        }
    }
}