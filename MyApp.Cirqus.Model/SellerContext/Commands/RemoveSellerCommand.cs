﻿#region using

using System;
using d60.Cirqus.Commands;

#endregion

namespace MyApp.Cirqus.Model.SellerContext.Commands
{
    public class RemoveSellerCommand : Command<Seller>
    {
        public RemoveSellerCommand(Guid aggregateRootId) : base(aggregateRootId.ToString())
        {
        }

        public override void Execute(Seller aggregateRoot)
        {
            aggregateRoot.Deletion = DateTimeOffset.UtcNow;
        }
    }
}