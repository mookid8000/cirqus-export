﻿#region using

using System;
using d60.Cirqus.Commands;
using MyApp.Cirqus.Model.SharedContext;

#endregion

namespace MyApp.Cirqus.Model.SellerContext.Commands
{
    public class UpdateSellerAddressCommand : Command<Seller>
    {
        private readonly Address _newAddress;

        public UpdateSellerAddressCommand(Guid aggregateRootId, Address newAddress) : base(aggregateRootId.ToString())
        {
            _newAddress = newAddress;
        }

        public override void Execute(Seller aggregateRoot)
        {
            aggregateRoot.Address = _newAddress;
        }
    }
}