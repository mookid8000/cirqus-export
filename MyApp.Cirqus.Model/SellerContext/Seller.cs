﻿#region using

using System;
using MyApp.Cirqus.Model.SharedContext;

#endregion

namespace MyApp.Cirqus.Model.SellerContext
{
    public sealed class Seller : Business
    {
        public Guid? SellerId { get; set; }

        public new string Id
        {
            get { return SellerId.ToString(); }
            set { SellerId = new Guid(value); }
        }
    }
}