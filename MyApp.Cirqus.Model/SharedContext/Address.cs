﻿#region using

using System;

#endregion

namespace MyApp.Cirqus.Model.SharedContext
{
    public struct Address : IEquatable<Address>
    {
        public CountryCode CountryCode { get; set; }
        public string FirstLine { get; set; }
        public string SecondLine { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string ProvinceOrState { get; set; }

        public override string ToString()
        {
            return $"{FirstLine}, {PostalCode}, {CountryCode}";
        }

        public bool Equals(Address other)
        {
            return CountryCode == other.CountryCode && string.Equals(FirstLine, other.FirstLine) &&
                   string.Equals(SecondLine, other.SecondLine) && string.Equals(PostalCode, other.PostalCode) &&
                   string.Equals(City, other.City) && string.Equals(ProvinceOrState, other.ProvinceOrState);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Address && Equals((Address) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) CountryCode;
                hashCode = (hashCode*397) ^ (FirstLine?.GetHashCode() ?? 0);
                hashCode = (hashCode*397) ^ (SecondLine?.GetHashCode() ?? 0);
                hashCode = (hashCode*397) ^ (PostalCode?.GetHashCode() ?? 0);
                hashCode = (hashCode*397) ^ (City?.GetHashCode() ?? 0);
                hashCode = (hashCode*397) ^ (ProvinceOrState?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}