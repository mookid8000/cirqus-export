﻿#region using

using d60.Cirqus.Events;
using MyApp.Cirqus.Model.BaseContext;
using MyApp.Cirqus.Model.SharedContext.Events;

#endregion

namespace MyApp.Cirqus.Model.SharedContext
{
    public abstract class Business : AggregateRootBase, IEmit<BusinessAddressUpdatedEvent>
    {
        private Address _address;

        public Address Address
        {
            get { return _address; }
            set { Emit(new BusinessAddressUpdatedEvent(value)); }
        }

        public void Apply(BusinessAddressUpdatedEvent e)
        {
            _address = e.Address;
        }
    }
}