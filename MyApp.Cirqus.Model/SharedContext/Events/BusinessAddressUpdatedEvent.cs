#region using

using d60.Cirqus.Events;

#endregion

namespace MyApp.Cirqus.Model.SharedContext.Events
{
    public class BusinessAddressUpdatedEvent : DomainEvent<Business>
    {
        public Address Address { get; private set; }

        public BusinessAddressUpdatedEvent(Address address)
        {
            Address = address;
        }
    }
}